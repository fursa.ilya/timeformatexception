package ru.studwork;

import java.util.Scanner;

/**
 * Создание и обработка собственных исключений.
 * В программе требуется:
 *
 * Создать собственное исключение (class).
 * Создать метод (throw), который может возбуждать это исключение(throws).
 * Написать метод, перехватывающий и обрабатывающий исключение (try-catch), возбуждаемое другим методом.
 * Исключение: с консоли вводится время суток (чч:мм:сс). Добиться ввода только правильного времени.
 */
public class Solution {

    public static void main(String[] args) throws TimeFormatException { //throws - означает что метод может выбросить исключение
        Scanner sc = new Scanner(System.in); //Класс обеспечивающий пользовательский ввод(советую почитать подробнее о нем)
        System.out.print("Введите чч: ");
        int hours = sc.nextInt();
        System.out.println("Введите мм: ");
        int minutes = sc.nextInt();
        System.out.print("Введите cc: ");
        int seconds = sc.nextInt();

        //Если часы > 23 или меньше 0 то выбрасываем исключение, то же самое и с минутами, секундами
        if(hours > 23 || hours < 0 || minutes > 60 || minutes < 0 || seconds > 60 || seconds < 0) {
            throw new TimeFormatException(); //Почитать про ключевое слово throws
        } else {
            System.out.println("Введенное время: " + hours + ":" + minutes + ":" + seconds); //Выводим полученную дату в случае если формат ввода был коррекнтным
        }
    }

}
