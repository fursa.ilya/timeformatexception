package ru.studwork;

/**
 * Создаем собственное исключение
 * унаследовав наш класс от Exception
 * Советую почитать главу любой книги по Java про исключения,
 * понимать что такое checked и unchecked исключения. 
 * Ориентироваться в иерархии классов исключений
 */
public class TimeFormatException extends Exception {

    @Override
    public String toString() {
        return "Wrong date format!";
    }
}
